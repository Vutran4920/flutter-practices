import 'package:flutter/material.dart';
import './login_screen.dart';
import './sw_screen.dart';

class StopwatchApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      routes:{
        '/': (context) => LoginScreen(),
        '/stopwatch': (context){
          return StopwatchScreen();
        }
      },
      initialRoute: '/',
    );
  }

}

