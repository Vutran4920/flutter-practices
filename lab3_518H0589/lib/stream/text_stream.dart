import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:math';

class TextStream {

  Stream<String> getText() async*{
    final String streamText = "This is a text, it will be repeated every 5 seconds";

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      return streamText;
    });
  }
}
