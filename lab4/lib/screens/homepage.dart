import 'package:flutter/material.dart';
import 'package:community_material_icon/community_material_icon.dart';

class HomePage extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text("TiKI.VN"),
            leading: IconButton(icon: Icon(Icons.menu), onPressed:() {},),
            actions: <Widget>[
              Stack(
                children: <Widget>[
                  IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {},),
                  Positioned(
                    child: Container(
                      width: 16.0,
                      height: 16.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.deepOrangeAccent,
                      ),
                        child: Text("4", style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold)),
                    ),
                  ),
                ],
              )
            ],

            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(120.0),
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Container(
                  height:48.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4.0),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 5.0,
                        offset: Offset(0.0, 0.0)
                      ),
                    ]
                  ),
                    child: Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.search, color: Colors.grey[600],),
                        onPressed: () {},
                      ),
                      Expanded(
                        child: Text("Sản phẩm, thương hiệu và mọi thứ bạn cần",
                          style: TextStyle(color:Colors.grey[600], fontSize:16.0),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),

                      IconButton(
                        icon: Icon(CommunityMaterialIcons.barcode_scan, color: Colors.grey[600],),
                        onPressed: () {},
                      ),
                   ],
                  ),
                ),
              ),
            ),
          ),
          SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate((context, index){
                return Container(color: Colors.red[(index%9)*100],);
              }),
              itemExtent: 56.0),
        ],
      )
    );
  }
}
