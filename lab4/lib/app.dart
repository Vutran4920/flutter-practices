import 'package:flutter/material.dart';
import 'package:lab4/screens/homepage.dart';

class MyTikiApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: "Tiki Clone App",
      debugShowCheckedModeBanner: false,
      home: HomePage()
    );
  }
}